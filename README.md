
## Build and run on port `3000` with `local` environment settings

`go build && SBOB_ENVIRONMENT=local SBOB_PORT=3000 ./staticbob`

## Steps to run

### Install `fresh`

`go get github.com/pilu/fresh`

### Run with `fresh` auto reloading

`SBOB_ENVIRONMENT=local fresh`
