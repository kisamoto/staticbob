package main

import (
	"strings"

	sbob "bitbucket.org/kisamoto/staticbob/api"
	log "github.com/Sirupsen/logrus"
	"github.com/gorilla/mux"
	"github.com/justinas/alice"
	"github.com/justinas/nosurf"
	"github.com/kelseyhightower/envconfig"
)

const (
	localEnvironment = "local"
)

type config struct {
	Environment string `default:"prod",required:"true"`
}

var conf config

func init() {

	err := envconfig.Process("sbob", &conf)
	if err != nil {
		log.Fatal(err.Error())
	}

	log.SetFormatter(&log.JSONFormatter{})
	if strings.ToLower(conf.Environment) == localEnvironment {
		log.SetFormatter(&log.TextFormatter{})
		log.SetLevel(log.DebugLevel)
	}
}

func main() {
	api := sbob.NewDefaultAPI()
	api.Debug(true)

	router := mux.NewRouter().StrictSlash(true)

	// commonHandlers := alice.New(sbob.LoggingMiddleware)
	commonHandlers := alice.New(sbob.LoggingMiddleware, sbob.RecoveryMiddleware)
	apiHandlers := commonHandlers.Extend(alice.New(sbob.AuthenticationMiddleware))
	webAppHandlers := commonHandlers.Extend(alice.New(nosurf.NewPure))

	v1 := router.PathPrefix("/api/v1").Subrouter()
	// Base level API routes
	sbob.AddRoutes(v1.PathPrefix("/site").Subrouter(),
		sbob.Routes{
			sbob.NewRouteHandler("CreateStatic", "POST", "/", apiHandlers, api.CreateStaticSite),
			sbob.NewRouteHandler("RetrieveStatic", "GET", "/", apiHandlers, api.RetrieveStaticSite),
			sbob.NewRouteHandler("UpdateStatic", "PUT", "/", apiHandlers, api.UpdateStaticSite),
			sbob.NewRouteHandler("DeleteStatic", "DELETE", "/", apiHandlers, api.DeleteStaticSite),
		})

	// Git Info API routes
	sbob.AddRoutes(v1.PathPrefix("/git").Subrouter(),
		sbob.Routes{
			sbob.NewRouteHandler("RetrieveGitInfo", "GET", "/", apiHandlers, api.RetrieveGitInfo),
		})

	// User info API routes
	sbob.AddRoutes(v1.PathPrefix("/user").Subrouter(),
		sbob.Routes{})

	// Website routes
	sbob.AddRoutes(router,
		sbob.Routes{
			sbob.NewRouteHandler("IndexGet", "GET", "/", webAppHandlers, api.HomePageGET),
			sbob.NewRouteHandler("IndexPost", "POST", "/", webAppHandlers, api.HomePagePOST),
			sbob.NewRouteHandler("SubmitGet", "GET", "/submit", webAppHandlers, api.NewSiteGET),
			sbob.NewRouteHandler("SubmitPost", "POST", "/submit", webAppHandlers, api.NewSitePOST),
			sbob.NewRouteHandler("AboutGet", "GET", "/about", webAppHandlers, api.AboutPage),
			sbob.NewRouteHandler("ContactGet", "GET", "/contact", webAppHandlers, api.ContactPageGET),
			sbob.NewRouteHandler("ContactPost", "POST", "/contact", webAppHandlers, api.ContactPagePOST),
			sbob.NewRouteHandler("PricingGet", "GET", "/pricing", webAppHandlers, api.PricingPage),
		})

	api.Run(router)
}
