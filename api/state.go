package staticbob

import "time"

type State struct {
	SiteID       string
	Revision     string
	LastModified time.Time
	Host         string
}
