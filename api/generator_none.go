package staticbob

// GeneratorNone takes an already built static site and just syncs it.
type GeneratorNone struct {

}

// Generate does nothing. The site is already generated. 
func (generator *GeneratorNone) Generate(dirpath string) (err error) {
  return
}
