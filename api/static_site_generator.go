package staticbob

type StaticSiteGenerator interface {
  Generate(dirpath string) (err error)
}
