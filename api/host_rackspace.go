package staticbob

// HostRackspace allows hosting on Rackspace cloud storage
type HostRackspace struct {
}

// Name returns "rackspace" to identify the host
func (host HostRackspace) Name() (name string) {
	return "rackspace"
}
