package staticbob

import (
	"encoding/json"
	"errors"
	"net/http"
	"strconv"
)

type StaticSite struct {
	ID                   string `json:"id,omitempty"`
	Generator            string `json:"generator"`
	RepositoryURL        string `json:"repository_url"`
	RepositoryBranch     string `json:"repository_branch"`
	RepositoryPrivate    bool   `json:"repository_private"`
	RepositoryPrivateKey string `json:"repository_private_key,omitempty"`
}

func NewStaticSiteFromRequest(r *http.Request) (site *StaticSite) {
	return &StaticSite{
		ID:                   r.FormValue("id"),
		Generator:            r.FormValue("generator"),
		RepositoryURL:        r.FormValue("repo_url"),
		RepositoryBranch:     r.FormValue("repo_branch"),
		RepositoryPrivateKey: r.FormValue("repo_key"),
	}
}

func NewStaticSiteFromArray(arr []string) (site *StaticSite, err error) {
	if len(arr) != 6 {
		return nil, errors.New(errorArrayLength)
	}
	repositoryPrivate, err := strconv.ParseBool(arr[5])
	if err != nil {
		return nil, err
	}
	return &StaticSite{
		ID:                   arr[0],
		Generator:            arr[1],
		RepositoryURL:        arr[2],
		RepositoryBranch:     arr[3],
		RepositoryPrivate:    repositoryPrivate,
		RepositoryPrivateKey: arr[5],
	}, nil
}

func (site *StaticSite) ToArray() []string {
	return []string{
		site.ID,
		site.Generator,
		site.RepositoryURL,
		site.RepositoryBranch,
		strconv.FormatBool(site.RepositoryPrivate),
		site.RepositoryPrivateKey,
	}
}

func (site *StaticSite) IsValid() bool {
	return site.Generator != "" &&
		site.RepositoryURL != "" &&
		site.RepositoryBranch != "" &&
		// If repository is private, must have a private key to use.
		(!site.RepositoryPrivate || site.RepositoryPrivateKey != "")
}

func (api *API) CreateStaticSite(w http.ResponseWriter, r *http.Request) {
	decoder := json.NewDecoder(r.Body)
	var site StaticSite
	err := decoder.Decode(&site)
	ErrorHandler(err)
	if err != nil {
		NewErrorResponse(w, "Unable to decode JSON")
		return
	}
	if !site.IsValid() {
		NewErrorResponse(w, "Invalid request")
		return
	}

	id, err := api.storage.SaveStaticSite(site)
	ErrorHandler(err)
	DebugStatement(string(id))
	NewSuccessResponse(w, string(id))
}

func (api *API) RetrieveStaticSite(w http.ResponseWriter, r *http.Request) {
	api.storage.RetrieveStaticStie("")
}

func (api *API) UpdateStaticSite(w http.ResponseWriter, r *http.Request) {
	site := NewStaticSiteFromRequest(r)
	api.storage.UpdateStaticSite(site.ID, *site)
}

func (api *API) DeleteStaticSite(w http.ResponseWriter, r *http.Request) {

}
