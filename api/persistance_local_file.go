package staticbob

import (
	"encoding/csv"
	"errors"
	"io"
	"os"
)

func NewLocalFilePersistance() Persistance {
	return NewLocalFilePersistanceWithPath("static_sites.csv")
}

func NewLocalFilePersistanceWithPath(filepath string) Persistance {
	f, err := os.OpenFile(filepath, os.O_APPEND|os.O_RDWR|os.O_CREATE, 0600)
	if err != nil {
		panic(err)
	}
	return &LocalFilePersistance{
		file: f,
	}
}

type LocalFilePersistance struct {
	file io.ReadWriter
}

func (storage *LocalFilePersistance) SaveStaticSite(staticSite StaticSite) (id string, err error) {

	staticSite.ID = storage.generateID()

	w := csv.NewWriter(storage.file)

	if err = w.Write(staticSite.ToArray()); err != nil {
		panic(err)
	}
	w.Flush()
	if err = w.Error(); err != nil {
		panic(err)
	}
	return staticSite.ID, nil
}

func (storage *LocalFilePersistance) UpdateStaticSite(id string, staticSite StaticSite) (success bool, err error) {
	return
}

func (storage *LocalFilePersistance) RetrieveStaticStie(id string) (site *StaticSite, err error) {
	r := csv.NewReader(storage.file)
	for {
		record, err := r.Read()
		if err == io.EOF {
			break
		}
		if err != nil {
			panic(err)
		}
		if id == record[0] {
			return NewStaticSiteFromArray(record)
		}
	}
	return nil, errors.New(errorSiteNotFound)
}

func (storage *LocalFilePersistance) generateID() (id string) {
	id = RandStringBytesMaskImprSrc(1)

	site, err := storage.RetrieveStaticStie(id)
	if err.Error() == errorSiteNotFound {
		return id
	}
	if err != nil {
		panic(err)
	}
	if site != nil {
		return storage.generateID()
	}
	return storage.generateID()
}
