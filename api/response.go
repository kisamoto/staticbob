package staticbob

import (
	"encoding/json"
	"net/http"
)

type Message struct {
	Message string `json:"message"`
}

type Response struct {
	Success  bool        `json:"success"`
	Content  interface{} `json:"content"`
	Messages []Message   `json:"messages"`
}

func NewSuccessResponse(w http.ResponseWriter, content interface{}) {
	NewSuccessResponseWithMessages(w, content, nil)
}

func NewSuccessResponseWithMessages(w http.ResponseWriter, content interface{}, messages []Message) {
	NewJSONResponse(w, true, content, messages)
}

func NewErrorResponse(w http.ResponseWriter, message string) {
	m := Message{Message: message}
	NewErrorResponseWithMessages(w, []Message{m})
}

func NewErrorResponseWithMessages(w http.ResponseWriter, messages []Message) {
	NewJSONResponse(w, false, nil, messages)
}

func NewJSONResponse(w http.ResponseWriter, success bool, content interface{}, messages []Message) {
	// Todo: Set the response type to "application/json"
	r := &Response{
		Success:  success,
		Content:  content,
		Messages: messages,
	}
	json.NewEncoder(w).Encode(r)
}
