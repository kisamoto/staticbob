package staticbob

import (
	"net/http"
	"time"

	log "github.com/Sirupsen/logrus"
)

var debug = false

func DebuggingEnabled() bool {
	return debug
}

func DebugStatement(format string, values ...interface{}) {
	if DebuggingEnabled() {
		log.WithFields(
			log.Fields{
				"service": "staticbob",
			}).Debugf(format, values...)
	}
}

func DebugRequest(r *http.Request, duration time.Duration) {
	if DebuggingEnabled() {
		log.WithFields(
			log.Fields{
				"service":  "staticbob",
				"method":   r.Method,
				"url":      r.URL.String(),
				"duration": duration.String(),
			}).Debug()
	}
}

func ErrorStatement(format string, values ...interface{}) {
	log.WithFields(
		log.Fields{
			"service": "staticbob",
		}).Errorf(format, values...)
}

func ErrorHandler(err error) {
	if err != nil {
		ErrorStatement("%v\n", err)
	}
}
