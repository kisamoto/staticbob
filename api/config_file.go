package staticbob

// ConfigFile represents a file in the base level of the static site repository
// that contains all information to deploy the site.
type ConfigFile struct {
	Host   string
	Region string
}

// ParseTOMLConfigFile takes a path to a TOML file and extracts it into a ConfigFile struct
// Todo: TOML is used in Hugo. Find other static site generator config formats.
// ref: https://blog.gopheracademy.com/advent-2014/reading-config-files-the-go-way/
func ParseTOMLConfigFile(path string) (config *ConfigFile) {
	return
}
