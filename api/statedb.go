package staticbob

type StateDatabase interface {
	FetchState(siteID string) (state State)
	UpsertState(siteID string, state State) (id string, err error)
}
