package staticbob

import (
	"net/http"

	"github.com/gorilla/mux"
	"github.com/justinas/alice"
)

type Route struct {
	Name        string
	Method      string
	Pattern     string
	HandlerFunc http.Handler
}

type Routes []Route

func AddRoutes(router *mux.Router, routes []Route) *mux.Router {

	for _, route := range routes {
		router.
			Methods(route.Method).
			Path(route.Pattern).
			Name(route.Name).
			Handler(route.HandlerFunc)
	}

	return router
}

func NewRouteHandler(name, method, pattern string, middleware alice.Chain, handler http.HandlerFunc) Route {
	return Route{
		Name:        name,
		Method:      method,
		Pattern:     pattern,
		HandlerFunc: middleware.ThenFunc(handler),
	}
}
