package staticbob

// HostGoogleCloudStorage allows hosting to Google Cloud Storage
type HostGoogleCloudStorage struct {
}

// Name returns "google-cloud-storage" to identify the host
func (host *HostGoogleCloudStorage) Name() (name string) {
	return "google-cloud-storage"
}
