package staticbob

import "net/http"

const (
	NoRepoSpecified        = "No repository URL specified."
	UnableToLookupBranches = "Unable to lookup branches for repository"
)

func NewGitInfo(url string) *GitInfo {
	return &GitInfo{
		URL: url,
	}
}

type GitInfo struct {
	URL      string
	Branches []string
}

func (info *GitInfo) FetchBranches() (err error) {
	return nil
}

func (api *API) RetrieveGitInfo(w http.ResponseWriter, r *http.Request) {
	repo := r.URL.Query().Get("repo")

	if repo == "" {
		NewErrorResponse(w, NoRepoSpecified)
		return
	}

	context := make(map[string]interface{})
	info := NewGitInfo(repo)
	err := info.FetchBranches()

	if err != nil {
		NewErrorResponse(w, UnableToLookupBranches)
		return
	}

	context["Info"] = info
	NewSuccessResponse(w, context)
}
