package staticbob

import (
	"net/http"

	"github.com/gorilla/mux"
	"github.com/justinas/nosurf"
)

// HomePageGET handles the homepage on a GET request.
func (api *API) HomePageGET(w http.ResponseWriter, r *http.Request) {
	context := make(map[string]string)
	context["Token"] = nosurf.Token(r)
	templates.ExecuteTemplate(w, "home.html", context)
}

// HomePagePOST handles a Git repo being submitted from the front page
func (api *API) HomePagePOST(w http.ResponseWriter, r *http.Request) {
	url, err := api.router.(*mux.Router).Get("SubmitPost").URL()
	ErrorHandler(err)

	q := r.URL.Query()
	q.Set("repo_url", r.FormValue("repo_url"))

	url.RawQuery = q.Encode()

	http.Redirect(w, r, url.String(), http.StatusFound)
}
