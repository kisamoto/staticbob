package staticbob

import "net/http"

func (api *API) PricingPage(w http.ResponseWriter, r *http.Request) {
	templates.ExecuteTemplate(w, "pricing.html", nil)
}

func (api *API) AboutPage(w http.ResponseWriter, r *http.Request) {
	templates.ExecuteTemplate(w, "about.html", nil)
}
