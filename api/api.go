package staticbob

import (
	"html/template"
	"net/http"
	"os"
	"path/filepath"

	log "github.com/Sirupsen/logrus"
	"github.com/leekchan/gtf"
)

const (
	defaultPort = ":3001"
)

var templates *template.Template

// custom template delimiters since the Go default delimiters clash
// with Angular's default.
var templateDelims = []string{"{%", "%}"}

// NewDefaultAPI creates a new API with LocalFilePersistance and no template directories
func NewDefaultAPI() *API {
	return NewAPI(
		NewLocalFilePersistance(),
		[]string{})
}

// NewAPI takes a storage backend an array of template directories to look for HTML templates.
func NewAPI(storage Persistance, templateDirs []string) *API {
	log.Info("Starting API")
	return &API{
		storage:      storage,
		templateDirs: append([]string{"templates"}, templateDirs...),
	}
}

// API is the barebones StaticBob API
type API struct {
	router       http.Handler
	templateDirs []string
	storage      Persistance
}

// Debug enables or disables debug mode.
func (api *API) Debug(enable bool) {
	debug = enable
}

// AddTemplateDirs adds a list of template directories and parses them
func (api *API) AddTemplateDirs(templateDirs []string) {
	api.templateDirs = append(api.templateDirs, templateDirs...)
	parseTemplateFiles(api.templateDirs)
}

// Run applies the http.Handler with the default port or a port set by the
// Environment Variable `PORT`.
func (api *API) Run(router http.Handler) {
	api.RunP(defaultPort, router)
}

// RunP takes a port and http.Handler and is a wrapper for `http.ListenAndServe`
func (api *API) RunP(port string, router http.Handler) {
	api.router = router
	parseTemplateFiles(api.templateDirs)
	// Overwrite any port if it's set in the environment. Mainly used for codegangsta/gin
	if p, present := os.LookupEnv("PORT"); present {
		port = p
	}
	log.Infof("Running on port %s", port)
	http.ListenAndServe(port, router)
}

func parseTemplateFiles(templateDirs []string) {
	cwd, _ := os.Getwd() // gives us the source path if we haven't installed.
	for _, t := range templateDirs {
		templateDir := filepath.Join(cwd, t)

		err := filepath.Walk(templateDir, func(path string, info os.FileInfo, err error) error {
			if err != nil {
				return err
			}
			// don't process folders themselves
			if info.IsDir() {
				return nil
			}
			templateName := path[len(templateDir):]
			if templates == nil {
				templates = gtf.New(templateName)
				templates.Delims(templateDelims[0], templateDelims[1])
				_, err = templates.ParseFiles(path)
			} else {
				_, err = templates.New(templateName).ParseFiles(path)
			}
			DebugStatement("Processed template %s", templateName)
			return err
		})
		if err != nil {
			log.Fatal(err)
		}
	}
}
