package staticbob

type Persistance interface {
	SaveStaticSite(staticSite StaticSite) (id string, err error)
	UpdateStaticSite(id string, staticSite StaticSite) (success bool, err error)
	RetrieveStaticStie(id string) (site *StaticSite, err error)
}
