package staticbob

import (
	"net/http"
	"strings"

	"github.com/asaskevich/govalidator"
	"github.com/justinas/nosurf"
)

func NewContactMessage(r *http.Request) *ContactMessage {
	return &ContactMessage{
		Token:   nosurf.Token(r),
		Email:   r.FormValue("email"),
		Name:    r.FormValue("name"),
		Content: r.FormValue("message"),
	}
}

type ContactMessage struct {
	Token   string
	Email   string
	Name    string
	Content string
	Errors  map[string]string
}

func (message *ContactMessage) IsValid() (isValid bool) {
	message.Errors = make(map[string]string)

	// Email validation
	if !govalidator.IsEmail(message.Email) {
		message.Errors["Email"] = "Please enter a valid email address"
	}

	// Check there are at least 100 chars
	if len(strings.TrimSpace(message.Content)) < 100 {
		DebugStatement("Length: %d", len(strings.TrimSpace(message.Content)))
		message.Errors["Content"] = "Please enter at least 100 chars"
	}

	return len(message.Errors) == 0
}

func (api *API) ContactPageGET(w http.ResponseWriter, r *http.Request) {
	templates.ExecuteTemplate(w, "contact.html", &ContactMessage{
		Token: nosurf.Token(r),
	})
}

func (api *API) ContactPagePOST(w http.ResponseWriter, r *http.Request) {
	message := NewContactMessage(r)

	if !message.IsValid() {
		// Pass the message back to prepopulate values and error messages.
		templates.ExecuteTemplate(w, "contact.html", message)
		return
	}

	// Save to a database & send an email.

	templates.ExecuteTemplate(w, "contact_success.html", nil)
}
