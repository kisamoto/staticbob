package staticbob

// HostS3 enables hosting on Amazon S3 cloud storage.
type HostS3 struct {
}

// Name returns "amazon-s3" to identify the host.
func (host *HostS3) Name() (name string) {
	return "amazon-s3"
}
