package staticbob

import (
	"net/http"

	"github.com/justinas/nosurf"
)

func (api *API) NewSiteGET(w http.ResponseWriter, r *http.Request) {
	context := make(map[string]interface{})
	context["Token"] = nosurf.Token(r)
	context["Site"] = StaticSite{
		RepositoryURL: r.URL.Query().Get("repo_url"),
	}
	templates.ExecuteTemplate(w, "new_repo.html", context)
}

func (api *API) NewSitePOST(w http.ResponseWriter, r *http.Request) {
	context := make(map[string]interface{})
	site := NewStaticSiteFromRequest(r)
	context["Token"] = nosurf.Token(r)
	context["Site"] = site
	templates.ExecuteTemplate(w, "new_repo.html", context)
}
