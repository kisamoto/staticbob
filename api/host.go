package staticbob

// Host represents a cloud storage provider to host the static site.
type Host interface {
	Name() string
	Sync(site StaticSite)
}
